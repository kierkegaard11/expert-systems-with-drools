package com.sample;

public class Osiguranje {

	// input
	private int snagaMotora;
	private int brojNezgoda;
	private int prethodniPremijskiStepen;
	private boolean nov;

	// output
	private double osnovnaCena = 0;
	private int noviPremijskiStepen = 0;
	private double konacnaCena = 0;

	public int getSnagaMotora() {
		return snagaMotora;
	}

	public void setSnagaMotora(int snagaMotora) {
		this.snagaMotora = snagaMotora;
	}

	public int getBrojNezgoda() {
		return brojNezgoda;
	}

	public void setBrojNezgoda(int brojNezgoda) {
		this.brojNezgoda = brojNezgoda;
	}

	public int getPrethodniPremijskiStepen() {
		return prethodniPremijskiStepen;
	}

	public void setPrethodniPremijskiStepen(int prethodniPremijskiStepen) {
		this.prethodniPremijskiStepen = prethodniPremijskiStepen;
	}

	public boolean isNov() {
		return nov;
	}

	public void setNov(boolean nov) {
		this.nov = nov;
	}

	public double getOsnovnaCena() {
		return osnovnaCena;
	}

	public void setOsnovnaCena(double osnovnaCena) {
		this.osnovnaCena = osnovnaCena;
	}

	public int getNoviPremijskiStepen() {
		return noviPremijskiStepen;
	}

	public void setNoviPremijskiStepen(int noviPremijskiStepen) {
		if (noviPremijskiStepen >= 1 && noviPremijskiStepen <= 12) {
			this.noviPremijskiStepen = noviPremijskiStepen;
		}

		if (noviPremijskiStepen < 1) {
			this.noviPremijskiStepen = 1;
		}

		if (noviPremijskiStepen > 12) {
			this.noviPremijskiStepen = 12;
		}
	}

	public double getKonacnaCena() {
		return konacnaCena;
	}

	public void setKonacnaCena(double konacnaCena) {
		this.konacnaCena = konacnaCena;
	}


	@Override
	public String toString() {
		return "Osiguranje [snagaMotora=" + snagaMotora + ", brojNezgoda=" + brojNezgoda + ", prethodniPremijskiStepen="
				+ prethodniPremijskiStepen + ", nov=" + nov + ", osnovnaCena=" + osnovnaCena + ", noviPremijskiStepen="
				+ noviPremijskiStepen + ", konacnaCena=" + konacnaCena + "]";
	}

}
