package com.sample;

public class Klima {

	//stavljamo zidna jer je to default
	private String tipUredjaja = "zidna";
	private String snaga = "nepoznato";
	// svejedno je da li je bool ili string
	private String inverter = "nepoznato";
	private int kvadratura;
	private boolean jednaProstorija;
	private String mogucnostUgradnjeNaZid = "npoznato";
	private String koriscenjeZaGrejanje = "nepoznato";
	private String tempNizeOdMinusPet = "nepoznato";


	public Klima() {
	}

	public String getTipUredjaja() {
		return tipUredjaja;
	}

	public void setTipUredjaja(String tipUredjaja) {
		this.tipUredjaja = tipUredjaja;
	}

	public String getSnaga() {
		return snaga;
	}

	public void setSnaga(String snaga) {
		this.snaga = snaga;
	}

	public String getInverter() {
		return inverter;
	}

	public void setInverter(String inverter) {
		this.inverter = inverter;
	}

	public int getKvadratura() {
		return kvadratura;
	}

	public void setKvadratura(int kvadratura) {
		this.kvadratura = kvadratura;
	}

	public String getMogucnostUgradnjeNaZid() {
		return mogucnostUgradnjeNaZid;
	}

	public void setMogucnostUgradnjeNaZid(String mogucnostUgradnjeNaZid) {
		this.mogucnostUgradnjeNaZid = mogucnostUgradnjeNaZid;
	}

	public String getKoriscenjeZaGrejanje() {
		return koriscenjeZaGrejanje;
	}

	public void setKoriscenjeZaGrejanje(String koriscenjeZaGrejanje) {
		this.koriscenjeZaGrejanje = koriscenjeZaGrejanje;
	}

	public String getTempNizeOdMinusPet() {
		return tempNizeOdMinusPet;
	}

	public void setTempNizeOdMinusPet(String tempNizeOdMinusPet) {
		this.tempNizeOdMinusPet = tempNizeOdMinusPet;
	}
	

	public boolean isJednaProstorija() {
		return jednaProstorija;
	}

	public void setJednaProstorija(boolean jednaProstorija) {
		this.jednaProstorija = jednaProstorija;
	}

	@Override
	public String toString() {
		return "Klima [tipUredjaja=" + tipUredjaja + ", snaga=" + snaga + ", inverter=" + inverter + ", kvadratura="
				+ kvadratura + ", brojProstorija=" +jednaProstorija + ", mogucnostUgradnjeNaZid="
				+ mogucnostUgradnjeNaZid + ", koriscenjeZaGrejanje=" + koriscenjeZaGrejanje + ", tempNizeOdMinusPet="
				+ tempNizeOdMinusPet + "]";
	}

}
