package com.sample;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

/**
 * This is a sample class to launch a rule.
 */
public class DroolsTest {

	public static final void main(String[] args) {
		try {
			// ostaje
			KieServices ks = KieServices.Factory.get();
			KieContainer kContainer = ks.getKieClasspathContainer();
			KieSession kSession = kContainer.newKieSession("ksession-rules");

			// iz teksta
			Klima k = new Klima();
			k.setKvadratura(62);
			k.setJednaProstorija(false);
			k.setTempNizeOdMinusPet("DA");

			// ostaje
			kSession.insert(k);
			kSession.fireAllRules();

			System.out.println(k);
		} catch (Throwable t) {
			t.printStackTrace();
		}
	}

}
